package code;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class Start
{
	public static void main(String[] args) throws InterruptedException, IOException
	{
		GpioController gpio = GpioFactory.getInstance();
		
		GpioPinDigitalOutput pin11 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "MyLED", PinState.LOW);
		GpioPinDigitalOutput pin13 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "MyLED", PinState.LOW);
		GpioPinDigitalOutput pin19 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_12, "MyLED", PinState.LOW);
		GpioPinDigitalOutput pin21 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_13, "MyLED", PinState.LOW);
		
		while (true)
			try
			{
				ServerSocket server = new ServerSocket(12345);
				
				log("--------------------------------------------------\nEn attente de connexion d'un client ..");
				
				Socket client = server.accept();
				OutputStream output = client.getOutputStream();
				InputStream inputStream = client.getInputStream();
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader input = new BufferedReader(inputStreamReader);
				
				log("Client connect� (" + client.getRemoteSocketAddress() + ")");
				
				String command;
				while ((command = input.readLine()) != null)
				{
					if (command != "")
						switch (command)
						{
							case "f":
								pin11.high();
								pin13.low();
								pin19.low();
								pin21.high();
								System.out.println(command + " -> avant");
								break;
							
							case "fr":
								pin11.high();
								pin13.low();
								pin19.low();
								pin21.low();
								System.out.println(command + " -> avant");
								break;
							
							case "fl":
								pin11.low();
								pin13.low();
								pin19.low();
								pin21.high();
								System.out.println(command + " -> avant");
								break;
							
							case "b":
								pin11.low();
								pin13.high();
								pin19.high();
								pin21.low();
								System.out.println(command + " -> arriere");
								break;
							
							case "br":
								pin11.low();
								pin13.high();
								pin19.low();
								pin21.low();
								System.out.println(command + " -> avant");
								break;
							
							case "bl":
								pin11.low();
								pin13.low();
								pin19.high();
								pin21.low();
								System.out.println(command + " -> avant");
								break;
							
							case "r":
								pin11.high();
								pin13.low();
								pin19.high();
								pin21.low();
								System.out.println(command + " -> avant");
								break;
							
							case "l":
								pin11.low();
								pin13.high();
								pin19.low();
								pin21.high();
								System.out.println(command + " -> avant");
								break;
							
							default:
								pin11.low();
								pin13.low();
								pin19.low();
								pin21.low();
								System.out.println(command + " -> stop");
								break;
						}
				}
				
				input.close();
				inputStreamReader.close();
				inputStream.close();
				output.close();
				client.close();
				server.close();
				
				log("Client d�connect�");
				
				pin11.low();
				pin13.low();
				pin19.low();
				pin21.low();
			}
			catch (IOException e)
			{
				log(e.getMessage());
			}
	}
	
	public static void log(String msg)
	{
		System.out.println(msg);
	}
}
