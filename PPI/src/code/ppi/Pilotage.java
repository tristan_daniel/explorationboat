package code.ppi;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class Pilotage extends Activity implements SensorEventListener {
	
	private String adresse = "192.168.10.1";
	private int port = 12345;
	
	private SensorManager senSensorManager;
	private Sensor senAccelerometer;
	
	private Socket socket;
	private OutputStream out;
	private InputStream in;
	
	private boolean connecte = false;
	
    private MjpegView mv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
		
		connexion();
		
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        mv = new MjpegView(this);
        setContentView(mv);        
        mv.setSource(MjpegInputStream.read("http://192.168.10.1:12346?action=stream"));
        mv.setDisplayMode(MjpegView.SIZE_BEST_FIT);
        mv.showFps(false);
		
		String arguments[] = getIntent().getExtras().getString("adresse").split(":");
		if(arguments.length == 2)
		{
			adresse = arguments[0];
			port = Integer.parseInt(arguments[1]);
		}
		
		senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	    senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    senSensorManager.registerListener(this, senAccelerometer , SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment
	{
		public PlaceholderFragment()
		{
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			View rootView = inflater.inflate(R.layout.fragment_pilotage, container, false);
			return rootView;
		}
	}
	
	public void connexion()
	{
		try
		{
			socket = new Socket(adresse, port);
			
			out = socket.getOutputStream();
			in = socket.getInputStream();
			
			connecte = true;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	public void deconnexion(View view)
	{
		onBackPressed();
	}
	
	private void deconnexion()
	{
		mv.stopPlayback();
		
		try
		{
			in.close();
			out.close();
			
			socket.close();
			
			connecte = false;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
	public void onBackPressed()
	{
		deconnexion();
		Intent intent = new Intent(this, Connexion.class);
		startActivity(intent);
	}

	@Override
	public void onAccuracyChanged(Sensor arg0, int arg1)
	{
		
	}

	@Override
	public void onSensorChanged(SensorEvent sensorEvent)
	{
		String direction = "";
		float v = - sensorEvent.values[0];
		float t = sensorEvent.values[1];
		
		if(v > 1f)
		{
			direction = "f";
		}
		else if(v < -1f)
		{
			direction = "b";
		}
		
		if(t > 1f)
		{
			direction += "r";
		}
		else if(t < -1f)
		{
			direction += "l";
		}
		
		send(direction);
		
	}
	
	private void send(String msg)
	{
		if(connecte)
			try
			{
				out.write((msg + "\n").getBytes());
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
	}
}
